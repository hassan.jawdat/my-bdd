"""AoC 2017 - Day 1 part 1"""

def inverse_captcha(sequence):
    result = 0
    for index, character in enumerate(sequence):
        if character == sequence[(index + 1) % len(sequence)]:
           result += int(character)
    return result 


if __name__ == "__main__":
    with open("input_day1") as file:
        data = file.read()            
    print(inverse_captcha(data))     