"""Day 1 part 1 feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
    parsers
)

from ..day_one_part_one import inverse_captcha

@scenario('features/day1_part1.feature', 'Sum all digits that match the next digit in the sequence')
def test_sum_all_digits_that_match_the_next_digit_in_the_sequence():
    """Sum all digits that match the next digit in the sequence."""

@given(parsers.parse('We have a starting {sequence}'), target_fixture="challenge_input")  
def we_have_a_starting_sequence(sequence):
    """We have a starting <sequence>."""
    return sequence


@when('I sum up all the digits that match the next digit in the sequence', target_fixture="captcha_result")
def i_sum_up_all_the_digits_that_match_the_next_digit_in_the_sequence(challenge_input):
    """I sum up all the digits that match the next digit in the sequence."""
    return inverse_captcha(challenge_input)


@then(parsers.parse('I will get a {result:d}'))
def i_will_get_a_result(captcha_result, result): 
    """I will get a <result>."""
    assert captcha_result == result  
 
