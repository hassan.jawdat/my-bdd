Feature: Day 1 part 1
    A reversed captcha. The captcha requires you to review a sequence of digits
    (your puzzle input) and find the sum of all digits that match the next digit
    in the list. The list is circular, so the digit after the last digit is the
    first digit in the list.

    Scenario Outline: Sum all digits that match the next digit in the sequence
        Given We have a starting <sequence>

        When I sum up all the digits that match the next digit in the sequence

        Then I will get a <result>
        
        Examples:
        | sequence | result |
        | 1122     |  3     |
        | 1111     |  4     |
        | 1234     |  0     | 
        | 91212129 |  9     |   